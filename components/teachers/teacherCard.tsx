import React from "react";
import {
    chakra,
    Box,
    Image,
    Flex,
    Icon,
    useColorModeValue,
} from "@chakra-ui/react";

import {MdHeadset, MdEmail, MdLocationOn} from "react-icons/md";

const TeacherCard = ({teacher}) => {

    let imageUrl = '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg';
    if (teacher.fields.image !== undefined) {
        imageUrl = teacher.fields.image[0].url
    }

    let fullName = teacher.fields.full_name
    let description = teacher.fields.description
    let city = teacher.fields.city
    let email = teacher.fields.email

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            w="full"
            alignItems="center"
            justifyContent="center"
        >
            <Box
                w="sm"
                mx="auto"
                bg={useColorModeValue("white", "gray.800")}
                shadow="lg"
                rounded="lg"
                overflow="hidden"
            >
                <Image
                    w="full"
                    h={56}
                    fit="cover"
                    objectPosition="center"
                    src={imageUrl}
                    alt="avatar"
                />

                <Box py={4} px={6}>
                    <chakra.h1
                        fontSize="xl"
                        fontWeight="bold"
                        color={useColorModeValue("gray.800", "white")}
                    >
                        {fullName}
                    </chakra.h1>

                    <chakra.p py={2} color={useColorModeValue("gray.700", "gray.400")}>
                        {description}
                    </chakra.p>

                    <Flex
                        alignItems="center"
                        mt={4}
                        color={useColorModeValue("gray.700", "gray.200")}
                    >
                        <Icon as={MdLocationOn} h={6} w={6} mr={2}/>

                        <chakra.h1 px={2} fontSize="sm">
                            {city}
                        </chakra.h1>
                    </Flex>
                    <Flex
                        alignItems="center"
                        mt={4}
                        color={useColorModeValue("gray.700", "gray.200")}
                    >
                        <Icon as={MdEmail} h={6} w={6} mr={2}/>

                        <chakra.h1 px={2} fontSize="sm">
                            {email}
                        </chakra.h1>
                    </Flex>
                </Box>
            </Box>
        </Flex>
    );
};

export default TeacherCard;
