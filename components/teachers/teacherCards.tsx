import React from "react";
import TeacherCard from "./teacherCard";


function TeacherCards({teachers}) {
    return (
        teachers.map(teacher => (
            <TeacherCard
                key={teacher.id}
                teacher={teacher}
            />
        ))
    )
}

export async function getStaticProps() {
    const res = await fetch('http://localhost:3000/ensemble/teachers')
    const teachers = await res.json()

    return {
        props: {teachers},
        revalidate: 100,
    }
}

export default TeacherCards
