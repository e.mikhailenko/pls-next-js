import React from "react";
import GalleryItem from "./galleryItem";

function GalleryItems({galleryItems, performances}) {
    return(
        galleryItems.map(galleryItem => (
            <GalleryItem
                key={galleryItem.id}
                galleryItem={galleryItem}
                performances={performances.filter(performance => performance.fields.gallery[0] === galleryItem.id)}
            />
        ))
    )
}

export async function getStaticProps() {
    const res = await fetch('http://localhost:3000/gallery')
    const [galleryItems, performances] = await res.json()

    return {
        props: {galleryItems, performances},
        revalidate: 10,
    }
}

export default GalleryItems
