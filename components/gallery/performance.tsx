import React from "react";
import {
    chakra,
    Flex,
    Icon,
    useColorModeValue, Link,
} from "@chakra-ui/react";

import {MdImage} from "react-icons/md";

const Performance = ({performance}) => {

    return (
        <Flex
            alignItems="center"
            mt={4}
            color={useColorModeValue("gray.700", "gray.200")}
        >
            <Icon as={MdImage} h={6} w={6} mr={2}/>
            <Link href={performance.fields.yandex_ref ? performance.fields.yandex_ref : ''}>
                <chakra.h1 px={2} fontSize="md">
                    {performance.fields.title}
                </chakra.h1>
            </Link>
        </Flex>
    );
};

export default Performance;
