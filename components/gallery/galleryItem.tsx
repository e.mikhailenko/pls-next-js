import React from "react";
import {
    chakra,
    Box,
    Image,
    Flex,
    useColorModeValue,
} from "@chakra-ui/react";
import Performance from "./performance";

const GalleryItem = ({galleryItem, performances}) => {

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            w="full"
            alignItems="center"
            justifyContent="center"
        >
            <Box
                w={{lg: "sm", md: "sm", sm: "sx"}}
                mx="auto"
                bg={useColorModeValue("white", "gray.800")}
                shadow="lg"
                rounded="lg"
                overflow="hidden"
            >
                <Image
                    w="full"
                    h={56}
                    fit="cover"
                    objectPosition="center"
                    src={galleryItem.fields.image ? galleryItem.fields.image[0].url : '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg'}
                    alt="avatar"
                />

                <Box py={4} px={6}>
                    <chakra.h1
                        fontSize="xl"
                        fontWeight="bold"
                        color={useColorModeValue("gray.800", "white")}
                    >
                        {galleryItem.fields.title}
                    </chakra.h1>

                    <chakra.p py={2} color={useColorModeValue("gray.700", "gray.400")}>
                        {galleryItem.fields.body}
                    </chakra.p>

                    {performances.map(performance => (
                        <Performance
                            key={performance.id}
                            performance={performance}
                        />
                    ))}
                </Box>
            </Box>
        </Flex>
    );
};

export default GalleryItem;
