import React, {useEffect, useState} from "react";
import {
    chakra,
    HStack,
    Link,
    Flex,
    IconButton,
    useColorModeValue,
    useDisclosure,
    CloseButton,
    VStack,
    Button,
    useColorMode,
    Image,
} from "@chakra-ui/react";
import {useViewportScroll} from "framer-motion";

import {AiFillHome, AiOutlineInbox, AiOutlineMenu} from "react-icons/ai";
import {BsFillCameraVideoFill} from "react-icons/bs";
import {FaMoon, FaSun} from "react-icons/fa";
import {Logo} from "@choc-ui/logo";

export default function Header() {
    const {toggleColorMode: toggleMode} = useColorMode();
    const text = useColorModeValue("dark", "light");
    const SwitchIcon = useColorModeValue(FaMoon, FaSun);
    const bg = useColorModeValue("white", "gray.800");
    const ref = React.useRef();
    const [y, setY] = React.useState(0);
    const height = 0;

    const {scrollY} = useViewportScroll();
    React.useEffect(() => {
        return scrollY.onChange(() => setY(scrollY.get()));
    }, [scrollY]);
    const cl = useColorModeValue("gray.800", "white");
    const mobileNav = useDisclosure();

    const linkStyles: any = {
        textDecoration: "none"
    }

    const MobileNavContent = (
        <VStack
            position={"fixed"}
            zIndex={999}
            top={0}
            left={0}
            right={0}
            display={mobileNav.isOpen ? "flex" : "none"}
            flexDirection="column"
            p={2}
            pb={4}
            m={2}
            bg={bg}
            spacing={3}
            rounded="sm"
            shadow="sm"
        >
            <CloseButton
                aria-label="Close menu"
                justifySelf="self-start"
                onClick={mobileNav.onClose}
            />
            <Link
                href="/ensemble"
                style={linkStyles}
            >
                <Button w="full" variant="ghost">
                    Ансамбль
                </Button>
            </Link>
            <Link
                href="/gallery"
                style={linkStyles}
            >
                <Button w="full" variant="ghost">
                    Галерея
                </Button>
            </Link>
            <Link
                href="/contacts"
                style={linkStyles}
            >
                <Button w="full" variant="ghost">
                    Контакты
                </Button>
            </Link>
            <Link
                href="/blog"
                style={linkStyles}
            >
                <Button w="full" variant="ghost">
                    Блог
                </Button>
            </Link>
            <Link
                href="/new_students"
                style={linkStyles}
            >
                <Button w="full" variant="ghost">
                    Новый набор
                </Button>
            </Link>
        </VStack>
    );

    return (
        <React.Fragment>
            <chakra.header
                position={"fixed"}
                zIndex={999}
                ref={ref}
                shadow={y > height ? "sm" : undefined}
                transition="box-shadow 0.2s"
                bg={bg}
                borderTopColor="brand.400"
                w="full"
                overflowY="hidden"
                borderBottomWidth={2}
                borderBottomColor={useColorModeValue("gray.200", "gray.900")}
            >
                <chakra.div
                    h="4.5rem"
                    mx="auto"
                    maxW="1200px"
                >
                    <Flex
                        w="full"
                        h="full"
                        px="6"
                        alignItems="center"
                        justifyContent="space-between"
                    >
                        <Flex align="flex-start">
                            <Link href="/">
                                <HStack>
                                    <Image
                                        w="200px"
                                        h="95px"
                                        p={5}
                                        display={{lg: "block", md: "none", sm: "none"}}
                                        src={useColorModeValue("/PLS_logo_black.png", "/logo.png")}
                                    />
                                </HStack>
                            </Link>
                        </Flex>
                        <Flex>
                            <HStack spacing="5" display={{base: "none", md: "flex"}}>
                                <Link
                                    href="/ensemble"
                                    style={linkStyles}
                                >
                                    <Button
                                        bg={bg}
                                        color="gray.500"
                                        display="inline-flex"
                                        alignItems="center"
                                        fontSize="md"
                                        _hover={{color: cl}}
                                        _focus={{boxShadow: "none"}}
                                    >
                                        Ансамбль
                                    </Button>
                                </Link>
                                <Link
                                    href="/gallery"
                                    style={linkStyles}
                                >
                                    <Button
                                        bg={bg}
                                        color="gray.500"
                                        display="inline-flex"
                                        alignItems="center"
                                        fontSize="md"
                                        _hover={{color: cl}}
                                        _focus={{boxShadow: "none"}}
                                    >
                                        Галерея
                                    </Button>
                                </Link>
                                <Link
                                    href="/contacts"
                                    style={linkStyles}
                                >
                                    <Button
                                        bg={bg}
                                        color="gray.500"
                                        display="inline-flex"
                                        alignItems="center"
                                        fontSize="md"
                                        _hover={{color: cl}}
                                        _focus={{boxShadow: "none"}}>Контакты
                                    </Button>
                                </Link>
                                <Link
                                    href="/blog"
                                    style={linkStyles}
                                >
                                    <Button
                                        bg={bg}
                                        color="gray.500"
                                        display="inline-flex"
                                        alignItems="center"
                                        fontSize="md"
                                        _hover={{color: cl}}
                                        _focus={{boxShadow: "none"}}
                                    >
                                        Блог
                                    </Button>
                                </Link>
                                <Link
                                    href="/new_students"
                                    style={linkStyles}
                                >
                                    <Button
                                        bg={bg}
                                        color="gray.500"
                                        display="inline-flex"
                                        alignItems="center"
                                        fontSize="md"
                                        _hover={{color: cl}}
                                        _focus={{boxShadow: "none"}}
                                    >
                                        Новый набор
                                    </Button>
                                </Link>
                            </HStack>
                        </Flex>
                        <Flex justify="flex-end" align="center" color="gray.400">
                            <IconButton
                                size="md"
                                fontSize="lg"
                                aria-label={`Switch to ${text} mode`}
                                variant="ghost"
                                color="current"
                                ml={{base: "0", md: "3"}}
                                onClick={toggleMode}
                                icon={<SwitchIcon/>}
                            />
                            <IconButton
                                display={{base: "flex", md: "none"}}
                                aria-label="Open menu"
                                fontSize="20px"
                                color={useColorModeValue("gray.800", "inherit")}
                                variant="ghost"
                                icon={<AiOutlineMenu/>}
                                onClick={mobileNav.onOpen}
                            />
                        </Flex>
                    </Flex>
                    {MobileNavContent}
                </chakra.div>
            </chakra.header>
        </React.Fragment>
    );
}
