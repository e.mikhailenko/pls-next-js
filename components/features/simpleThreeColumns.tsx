import React, {useEffect, useState} from "react";
import {
    SimpleGrid,
    Flex,
    useColorModeValue, Box,
} from "@chakra-ui/react";
import {base} from "../base";
import Column from "./column";

export default function SimpleThreeColumns() {

    const [columns, setColumn] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            await base('simple_three_columns')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setColumn(records);
                    fetchNextPage();
                });
        }
        fetchData().then();
    }, [])

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            p={{xl: 20, lg: 10, md: 10, sm: 10}}
            justifyContent="center"
            alignItems="center"
        >
            <Box
                shadow="xl"
                bg={useColorModeValue("white", "gray.800")}
                p={10}
                mx="auto"
                rounded={"lg"}
                m={10}
                w={{base: 345, lg: 1000, md: 610, sm: 200, xs: 100}}
            >
                <SimpleGrid
                    columns={{base: 1, md: 2, lg: 3}}
                    spacing={10}
                    px={{base: 10, lg: 16, xl: 24}}
                    py={{base: 10, lg: 16, xl: 24}}
                    // bg={useColorModeValue("white", "gray.800")}
                >
                    {columns.map(column => (
                        <Column
                            key={column.id}
                            column={column}
                        />
                    ))}
                </SimpleGrid>
            </Box>
        </Flex>
    );
}
