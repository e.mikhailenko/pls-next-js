import React, {useEffect, useState} from "react";
import {
    chakra,
    Box,
    SimpleGrid,
    Flex,
    useColorModeValue,
    Icon,
} from "@chakra-ui/react";
import {MdEmail, MdLocationOn, MdPhone} from "react-icons/md";

const Column = ({column}) => {

    const icon = column.fields.icon !== undefined ? column.fields.icon : ''
    const title = column.fields.title !== undefined ? column.fields.title : ''
    const body = column.fields.body !== undefined ? column.fields.body : ''

    const iconsList = {
        "phone": MdPhone,
        "email": MdEmail,
        "location": MdLocationOn
    }

    return (
        <Box>
            <Icon
                as={iconsList[icon]}
                boxSize={12}
                color={useColorModeValue("gray.900", "gray.100")}
                mb={4}
            />
            <chakra.h3
                mb={3}
                fontSize="lg"
                lineHeight="shorter"
                fontWeight="bold"
                color={useColorModeValue("gray.900", "gray.100")}
            >
                {title}
            </chakra.h3>
            <chakra.p
                lineHeight="tall"
                color={useColorModeValue("gray.600", "gray.400")}
            >
                {body}
            </chakra.p>
        </Box>
    );
}


export default Column
