import React from "react";
import {
    chakra,
    Box,
    useColorModeValue,
    Flex,
    SimpleGrid,
    Icon,
    Image,
    Button, Link,
} from "@chakra-ui/react";

import {FiExternalLink} from "react-icons/fi";

const CallToAction = ({callToAction}) => {

    const image = callToAction.fields !== undefined ? callToAction.fields.image : ''
    const label = callToAction.fields !== undefined ? callToAction.fields.label : ''
    const body = callToAction.fields !== undefined ? callToAction.fields.body : ''
    const buttonLabel = callToAction.fields !== undefined ? callToAction.fields.button_label : ''
    const href = callToAction.fields !== undefined ? callToAction.fields.href : ''

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            p={{xl: 20, lg: 10, md: 10, sm: 10}}
            justifyContent="center"
            alignItems="center"
        >
            <Box
                shadow="xl"
                bg={useColorModeValue("white", "gray.800")}
                p={10}
                mx="auto"
                rounded={"lg"}
                m={10}
                w={{base: 345, lg: 1000, md: 610, sm: 200, xs: 100}}
            >
                <SimpleGrid columns={{base: 1, md: 2}}>
                    <Image
                        src={image ? image[0].url : ''}
                        fit="cover"
                        w="full"
                        h={{base: 64, md: "full"}}
                        bg="gray.100"
                        loading="lazy"
                        rounded={"lg"}
                    />
                    <Flex
                        direction="column"
                        alignItems="center"
                        justifyContent="center"
                        px={{base: 4, md: 8, lg: 20}}
                        py={{base: 4, md: 8, lg: 20}}
                    >
                        <chakra.h1
                            mb={4}
                            fontSize={{base: "4xl", md: "4xl", lg: "5xl", sm: "lg"}}
                            fontWeight="bold"
                            color={useColorModeValue("brand.600", "gray.300")}
                            lineHeight="shorter"
                            textShadow="2px 0 currentcolor"
                        >
                            {label}
                        </chakra.h1>
                        <chakra.p
                            // pr={{base: 0, lg: 16}}
                            mb={4}
                            fontSize="lg"
                            color={useColorModeValue("brand.600", "gray.400")}
                            letterSpacing="wider"
                        >
                            {body}
                        </chakra.p>
                        <Link
                            href={href}
                            style={{textDecoration: "none"}}
                        >
                            <Button
                                my={5}
                                p={8}
                                display="inline-flex"
                                alignItems="center"
                                justifyContent="center"
                                px={5}
                                fontWeight="bold"
                                fontSize={{lg: "lg", md: "md", sm: "sm"}}
                                w="full"
                                rounded="md"
                                color={useColorModeValue("gray.900", "gray.100")}
                                bg={useColorModeValue("gray.300", "gray.500")}
                                _hover={{bg: useColorModeValue("gray.400", "gray.600")}}
                            >
                                {buttonLabel}
                                <Icon as={FiExternalLink} ml={2}/>
                            </Button>
                        </Link>
                    </Flex>
                </SimpleGrid>
            </Box>
        </Flex>
    );
};

export default CallToAction;
