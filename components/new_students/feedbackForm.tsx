import React, {useState} from "react";
import {
    chakra,
    Box,
    useColorModeValue,
    SimpleGrid,
    GridItem,
    Stack,
    FormControl,
    FormLabel,
    Input,
    Button,
    Select,
    FormErrorMessage,
    Alert,
    AlertIcon, CloseButton
} from "@chakra-ui/react";
import {base} from "../base";

export default function FeedbackForm() {

    const [fullChildName, setFullChildName] = useState(null)
    const [fullParentName, setFullParentName] = useState(null)
    const [childBirthDate, setChildBirthDate] = useState(null)
    const [phoneNumber, setPhoneNumber] = useState(null)
    const [message, setMessage] = useState(null)

    const [validateErrors, setValidateErrors] = useState({})
    const [displayAlert, setDisplayAlert] = useState("none")

    const sendData = () => {
        base('new_students').create([
            {
                "fields": {
                    "title": `student_${new Date().getTime()}`,
                    "full_child_name": fullChildName,
                    "full_parent_name": fullParentName,
                    "child_birth_date": childBirthDate,
                    "phone_number": phoneNumber
                }
            }
        ], function (err, records) {
            if (err) {
                console.error(err);
                return;
            }
            records.forEach(function (record) {
                setDisplayAlert(null)
                console.log(record.getId());
            });
        });
    }

    const submitForm = () => {

        let errorList = []

        if (fullChildName === '' || fullChildName === null) {
            errorList['fullChildName'] = 'Пожалуйста, заполните полное имя ребёнка'
        } else if (!fullChildName.match(/^[а-яА-Я]+$/)) {
            errorList['fullChildName'] = 'Полное имя должно состоять только из букв'
        }

        if (fullParentName === '' || fullParentName === null) {
            errorList['fullParentName'] = 'Пожалуйста, заполните полное имя родителя'
        } else if (!fullParentName.match(/^[а-яА-Я]+$/)) {
            errorList['fullParentName'] = 'Полное имя должно состоять только из букв'
        }

        if (childBirthDate === '' || childBirthDate === null) {
            errorList['childBirthDate'] = 'Пожалуйста, заполните дату рождения ребёнка'
        }
        if (phoneNumber === '' || phoneNumber === null) {
            errorList['phoneNumber'] = 'Пожалуйста, заполните номер телефона'
        }

        // spam check
        if (message !== null) {
            errorList['message'] = 'spam'
        }

        setValidateErrors(errorList)

        if (Object.keys(errorList).length === 0) {
            console.log('send data')
            sendData();
        } else {
            setDisplayAlert("none")
        }

    }

    return (
        <Box
            bg={useColorModeValue("gray.50", "inherit")}
            p={10}
            mt={20}
        >
            <Alert
                status="success"
                fontSize={"lg"}
                rounded={"lg"}
                mb={5}
                p={5}
                display={displayAlert ? displayAlert : null}
            >
                Данные успешно отправлены!
                <CloseButton
                    position="absolute"
                    right="17px"
                    top="18px"
                    alignContent={"center"}
                    onClick={() => setDisplayAlert("none")}
                />
            </Alert>
            <Box
                bg={useColorModeValue("gray.50", "inherit")}
                mt={[10, 0]}
                shadow="lg"
                w={{lg: 900, md: 600, sm: 400}}
            >
                <SimpleGrid spacing={{md: 6}} mb={200}>
                    <GridItem
                        mt={[5, null, 0]}
                        colSpan={{md: 2}}
                    >
                        <Stack
                            px={4}
                            py={5}
                            p={[null, 6]}
                            bg={useColorModeValue("white", "gray.700")}
                            spacing={6}
                            roundedTop="lg"
                        >
                            <chakra.h1
                                fontSize={"4xl"}
                                textAlign={"center"}
                                mb={4}
                            >
                                Новый набор
                            </chakra.h1>
                            <SimpleGrid columns={6} spacing={6}>
                                <FormControl
                                    as={GridItem}
                                    colSpan={6}
                                    isRequired
                                    isInvalid={validateErrors['fullChildName']}
                                >
                                    <FormLabel
                                        // for="full_child_name"
                                        fontSize="lg"
                                        fontWeight="lg"
                                        color={useColorModeValue("gray.700", "gray.50")}
                                    >
                                        Полное имя ребёнка
                                    </FormLabel>
                                    <Input
                                        type="text"
                                        name="full_child_name"
                                        id="full_child_name"
                                        autoComplete="full-child-name"
                                        mt={1}
                                        focusBorderColor="brand.400"
                                        shadow="lg"
                                        size="lg"
                                        w="full"
                                        rounded="lg"
                                        onChange={(e) => setFullChildName(e.target.value)}
                                    />
                                    <FormErrorMessage>{validateErrors['fullChildName']}</FormErrorMessage>
                                </FormControl>

                                <FormControl
                                    as={GridItem}
                                    colSpan={6}
                                    isRequired
                                    isInvalid={validateErrors['fullParentName']}
                                >
                                    <FormLabel
                                        // for="full_parent_name"
                                        fontSize="lg"
                                        fontWeight="lg"
                                        color={useColorModeValue("gray.700", "gray.50")}
                                    >
                                        Полное имя родителя
                                    </FormLabel>
                                    <Input
                                        type="text"
                                        name="full_parent_name"
                                        id="full_parent_name"
                                        autoComplete="full-parent-name"
                                        mt={1}
                                        focusBorderColor="brand.400"
                                        shadow="lg"
                                        size="lg"
                                        w="full"
                                        rounded="lg"
                                        onChange={(e) => setFullParentName(e.target.value)}
                                    />
                                    <FormErrorMessage>{validateErrors['fullParentName']}</FormErrorMessage>
                                </FormControl>

                                <FormControl
                                    as={GridItem}
                                    colSpan={[6, 3]}
                                    isRequired
                                    isInvalid={validateErrors['childBirthDate']}
                                >
                                    <FormLabel
                                        fontSize="lg"
                                        fontWeight="lg"
                                        // for="child_birth_date"
                                        color={useColorModeValue("gray.700", "gray.50")}
                                    >
                                        Дата рождения ребёнка
                                    </FormLabel>
                                    <Input
                                        type="date"
                                        name="child_birth_date"
                                        id="child_birth_date"
                                        autoComplete="child-birth-date"
                                        mt={1}
                                        focusBorderColor="brand.400"
                                        shadow="lg"
                                        size="lg"
                                        w="full"
                                        rounded="lg"
                                        onChange={(e) => setChildBirthDate(e.target.value)}
                                    />
                                    <FormErrorMessage>{validateErrors['childBirthDate']}</FormErrorMessage>
                                </FormControl>

                                <FormControl
                                    as={GridItem}
                                    colSpan={[6, 3]}
                                    isRequired
                                    isInvalid={validateErrors['phoneNumber']}
                                >
                                    <FormLabel
                                        fontSize="lg"
                                        fontWeight="lg"
                                        // for="phone_number"
                                        color={useColorModeValue("gray.700", "gray.50")}
                                    >
                                        Номер телефона
                                    </FormLabel>
                                    <Input
                                        type="number"
                                        name="phone_number"
                                        id="phone_number"
                                        autoComplete="phone-number"
                                        mt={1}
                                        focusBorderColor="brand.400"
                                        shadow="lg"
                                        size="lg"
                                        w="full"
                                        rounded="lg"
                                        onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                                    <FormErrorMessage>{validateErrors['phoneNumber']}</FormErrorMessage>
                                </FormControl>

                                <Input
                                    type="text"
                                    name="message"
                                    id="message"
                                    autoComplete="message"
                                    mt={1}
                                    focusBorderColor="brand.400"
                                    shadow="lg"
                                    size="lg"
                                    w="full"
                                    rounded="lg"
                                    display={"none"}
                                    onChange={(e) => setMessage(e.target.value)}
                                />

                            </SimpleGrid>
                        </Stack>
                        <Box
                            px={{base: 4, sm: 6}}
                            py={3}
                            bg={useColorModeValue("gray.100", "gray.900")}
                            textAlign="right"
                            roundedBottom="lg"
                        >
                            <Button
                                _focus={{shadow: ""}}
                                fontWeight={"lg"}
                                fontSize={"lg"}
                                color={useColorModeValue("gray.500", "gray.1000")}
                                onClick={submitForm}
                            >
                                Отправить
                            </Button>
                        </Box>
                    </GridItem>
                </SimpleGrid>
            </Box>
        </Box>
    );
}
