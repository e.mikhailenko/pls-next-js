import React from "react";
import {
    chakra,
    Box,
    Image,
    Flex,
    useColorModeValue,
    Link, Button,
} from "@chakra-ui/react";

const Post = ({post}) => {

    return (
            <Box
                w={{lg: "sm", md: "sm", sm: "sx"}}
                mx="auto"
                py={4}
                px={8}
                bg={useColorModeValue("white", "gray.800")}
                shadow="lg"
                rounded="lg"
            >
                <Flex justifyContent={{base: "center", md: "end"}} mt={-16}>
                    <Image
                        w={20}
                        h={20}
                        fit="cover"
                        rounded="full"
                        borderStyle="solid"
                        borderWidth={2}
                        borderColor={useColorModeValue("brand.500", "brand.400")}
                        alt="Testimonial avatar"
                        src={post.fields.image ? post.fields.image[0].url : '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg'}
                    />
                </Flex>

                <chakra.h2
                    color={useColorModeValue("gray.800", "white")}
                    fontSize={{base: "2xl", md: "3xl"}}
                    mt={{base: 2, md: 0}}
                    fontWeight="bold"
                >
                    {post.fields.title}
                </chakra.h2>

                <Box
                    mt={2}
                    fontSize={{base: "xl", md: "lg"}}
                    color={useColorModeValue("gray.600", "gray.200")}
                >
                    {`${post.fields.body.substring(0, 200)} ...`}
                </Box>

                <Flex justifyContent="end" mt={4}>
                    <Link
                        href={`/blog/post/${post.id}`}
                        style={{textDecoration: "none"}}
                    >
                        <Button
                            color={useColorModeValue("gray.600", "white")}
                            fontSize="lg"
                        >
                            Прочитать
                        </Button>
                    </Link>
                </Flex>
            </Box>
    );
};

export default Post;
