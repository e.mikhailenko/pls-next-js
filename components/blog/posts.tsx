import React from "react";
import Post from "./post";

function Posts ({posts}) {
    return(
        posts.map(post => (
            <Post
                key={post.id}
                post={post}
            />
        ))
    )
}

export async function getStaticProps() {
    const res = await fetch('http://localhost:3000/blog')
    const posts = await res.json()

    return {
        props: {posts},
        revalidate: 10,
    }
}

export default Posts
