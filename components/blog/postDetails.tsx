import React, {useEffect, useState} from "react";
import {
    chakra,
    Box,
    Image,
    Flex,
    useColorModeValue,
    Link,
} from "@chakra-ui/react";
import {base} from "../base";

const PostDetails = () => {

    const [post, setPost] = useState(null)
    const image = post ? post.fields.image : ''

    useEffect(() => {

        let splitPath = window.location.pathname.split('/')
        const id = splitPath[splitPath.length - 1]

        const fetchData = async () => {
            await base('posts').find(id, function (err, record: any) {
                if (err) {
                    console.error(err)
                    return
                } else {
                    console.log('Retrieved', record.id);
                    setPost(record)
                }
            })
        }
        fetchData().then();
    }, [])

    return (
        <>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                // p={10}
                w="full"
                alignItems="center"
                justifyContent="center"
            >
                <Box
                    mx="auto"
                    rounded="lg"
                    shadow="lg"
                    bg={useColorModeValue("white", "gray.800")}
                    maxW={"6xl"}
                >
                    <Image
                        roundedTop="lg"
                        w="full"
                        maxH={"2xl"}
                        fit="cover"
                        src={image ? image[0].url : '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg'}
                        alt="Article"
                    />

                    <Box p={10}>
                        <Box>
                            <chakra.h1
                                display="block"
                                color={useColorModeValue("gray.800", "white")}
                                fontWeight="bold"
                                fontSize="4xl"
                                mt={2}
                                textAlign={"center"}
                            >
                                {post ? post.fields.title : ''}
                            </chakra.h1>
                            <chakra.p
                                style={{flexDirection: 'row', flex: 1, flexWrap: 'wrap', flexShrink: 1}}
                                mt={2}
                                fontSize="lg"
                                color={useColorModeValue("gray.600", "gray.400")}
                            >
                                {post ? post.fields.body : ''}
                            </chakra.p>
                        </Box>

                    </Box>
                </Box>
            </Flex>
        </>
    )
}

export default PostDetails

