import React from "react";

import {
    Box, Button, chakra,
    Flex, Icon, Link, SimpleGrid,
    useColorModeValue,
} from "@chakra-ui/react";
import StructureImage from "./structureImage";
import {FiExternalLink} from "react-icons/fi";

const StructureImages = ({structureImages}) => {

    const images = structureImages.fields !== undefined ? structureImages.fields.images : []
    const label = structureImages.fields !== undefined ? structureImages.fields.label : ''

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            p={{xl: 20, lg: 10, md: 10, sm: 10}}
            justifyContent="center"
            alignItems="center"
        >
            <Box
                shadow="xl"
                bg={useColorModeValue("white", "gray.800")}
                p={10}
                mx="auto"
                rounded={"lg"}
                m={10}
                w={{base: 345, lg: 1000, md: 610, sm: 200, xs: 100}}
            >
                <SimpleGrid
                    alignItems="center"
                    spacing={10}
                    // columns={{base: 1}}
                    // spacingX={{base: 10, md: 24}}
                    mb={20}
                    mx="auto"
                >
                    <chakra.h2
                        mb={4}
                        fontSize={{base: "2xl", md: "4xl"}}
                        fontWeight="bold"
                        textAlign={{base: "center"}}
                        color={useColorModeValue("gray.900", "gray.400")}
                    >
                        {label}
                    </chakra.h2>
                    {images.map(image => (
                        <StructureImage
                            key={image.id}
                            image={image}
                        />
                    ))}
                </SimpleGrid>
                <Link
                    href={"/ensemble/educational_program"}
                    style={{textDecoration: "none"}}
                >
                    <Button
                        mt={2}
                        p={8}
                        display="inline-flex"
                        alignItems="center"
                        justifyContent="center"
                        px={5}
                        fontWeight="bold"
                        fontSize={{lg: "lg", md: "md", sm: "sm"}}
                        w="full"
                        rounded="md"
                        color={useColorModeValue("gray.900", "gray.100")}
                        bg={useColorModeValue("gray.300", "gray.500")}
                        _hover={{bg: useColorModeValue("gray.400", "gray.600")}}
                    >
                        Подробнее
                        <Icon as={FiExternalLink} ml={2}/>
                    </Button>
                </Link>
            </Box>
        </Flex>
    );
}

export default StructureImages
