import React from "react";
import {Image} from "@chakra-ui/react";

const StructureImage = ({image}) => {

    return (
            <Image
                w="full"
                h="full"
                rounded={"lg"}
                fit="cover"
                shadow={"xl"}
                src={image ? image.url : ''}
            />
    );
};

export default StructureImage;
