import React from "react";
import {chakra, Box, Flex, useColorModeValue, Link} from "@chakra-ui/react";

const Card = ({card}) => {

    let imageUrl = '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg';
    if (undefined !== card.fields.image) {
        imageUrl = card.fields.image[0].url
    }

    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            // p={50}
            // w="full"
            alignItems="center"
            justifyContent="center"
        >
            <Box
                bg={useColorModeValue("white", "gray.800")}
                // mx={{lg: 8}}
                // display={{lg: "flex", md: "flex", sm: "flex"}}
                display={"flex"}
                // maxW={{lg: "8xl"}}
                shadow={{lg: "lg", md: "lg", sm: "lg"}}
                w={{lg: "1000px", md: "500px", sm: "200px"}}
                h={{lg: "500px", md: "400px", sm: "200px"}}
                rounded="lg"
            >
                <Box
                    w={{lg: "100%", md: "100%", sm: "100%"}}
                >
                    <Box
                        h={{lg: "full", md: "full", sm: "full"}}
                        rounded={{base: "lg", lg: "lg", md: "lg", sm: "lg"}}
                        bgSize="cover"
                        style={{backgroundImage: `url(${imageUrl})`,}}
                    />
                </Box>

                <Box
                    py={12}
                    px={6}
                    maxW={{base: "xl", lg: "5xl", md: "5xl", sm: "5xl"}}
                    w={{lg: "50%"}}
                >
                    <chakra.h2
                        fontSize={{base: "2xl", md: "3xl"}}
                        color={useColorModeValue("gray.800", "white")}
                        fontWeight="bold"
                    >
                        {card.fields.label}
                    </chakra.h2>
                    <chakra.p
                        mt={4}
                        color={useColorModeValue("gray.600", "gray.400")}
                    >
                        {card.fields.description}
                    </chakra.p>

                    <Box
                        mt={8}
                    >
                        <Link
                            bg="gray.900"
                            color="gray.100"
                            px={5}
                            py={3}
                            fontWeight="semibold"
                            rounded="lg"
                            _hover={{bg: "gray.600"}}
                            href={`/ensemble/${card.fields.title}`}
                        >
                            Подробнее
                        </Link>
                    </Box>
                </Box>
            </Box>
        </Flex>
    );
};

export default Card;
