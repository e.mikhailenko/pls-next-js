import React from "react";
import Card from "./card";

function Cards ({cards}) {
    return(
        cards.map(card => (
            <Card
                key={card.id}
                card={card}
            />
        ))
    )
}

export async function getStaticProps() {
    const res = await fetch('http://localhost:3000/ensemble')
    const cards = await res.json()

    return {
        props: {cards},
        revalidate: 10,
    }
}

export default Cards
