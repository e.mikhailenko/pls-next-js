import React, {useEffect, useState} from "react";
import {
    Text,
    Box,
    Flex,
    useColorModeValue,
    Image,
    HStack,
    Stack,
} from "@chakra-ui/react";


const Slide = ({slide, sid, slidesCount}) => {

    return (
        <Box
            key={`slide-${sid}`}
            boxSize="full"
            shadow="md"
            flex="none"
            // mt={20}
        >
            <Image
                src={slide.img}
                boxSize="full"
                backgroundSize="cover"
                objectFit="cover"
            />
            <Stack
                p="8px 12px"
                pos="absolute"
                bottom="24px"
                textAlign="center"
                w="full"
                mb="8"
                color="white"
            >
                <Text fontSize="2xl">{slide.label}</Text>
                <Text fontSize="lg">{slide.description}</Text>
            </Stack>
        </Box>
    );
}

export async function getStaticProps() {
    const res = await fetch('http://localhost:3000')
    const [slide, sid, slidesCount] = await res.json()

    return {
        props: {slide, sid, slidesCount},
        revalidate: 10,
    }
}

export default Slide;
