import React, {useEffect, useState} from "react";
import {
    Text,
    Box,
    Flex,
    useColorModeValue,
    Image,
    HStack,
    Stack,
} from "@chakra-ui/react";
import {TextProps} from "@chakra-ui/layout";
import {base} from "../base";
import Slide from './slide'


const Carousel = () => {

    const [carouselImages, setCarouselImages] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await base('carousel_images')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setCarouselImages(records);
                    fetchNextPage();
                });
        }
        fetchData().then();
    }, [])


    function mainCarouselImages(carouselImage) {
        return carouselImage.fields.carousel[0] === 'recsvAJVKdVnY4WCb' && carouselImage.fields.image !== undefined;
    }

    const carouselImagesFilter = carouselImages.filter(mainCarouselImages);

    const slides = carouselImagesFilter.map(carouselImage => (
        {
            img: carouselImage.fields.image[0].url,
            // img: carouselImage.fields.image[0].thumbnails.large.url,
            label: carouselImage.fields.text_label,
            description: carouselImage.fields.text_label,
        }
    ));

    const arrowStyles: TextProps = {
        cursor: "pointer",
        pos: "absolute",
        top: "50%",
        w: "auto",
        mt: "-22px",
        p: "16px",
        color: "white",
        fontWeight: "bold",
        fontSize: "18px",
        transition: "0.6s ease",
        borderRadius: "0 3px 3px 0",
        userSelect: "none",
        _hover: {
            opacity: 0.8,
            bg: "black",
        },
    };

    const [currentSlide, setCurrentSlide] = useState(0);

    const slidesCount = slides.length;

    const prevSlide = () => {
        setCurrentSlide((s) => (s === 0 ? slidesCount - 1 : s - 1));
    };
    const nextSlide = () => {
        setCurrentSlide((s) => (s === slidesCount - 1 ? 0 : s + 1));
    };
    const setSlide = (slide) => {
        setCurrentSlide(slide);
    };
    const carouselStyle = {
        transition: "all .5s",
        ml: `-${currentSlide * 100}%`,
    };

    return (
        <Flex
            w="full"
            bg={useColorModeValue("gray.200", "gray.600")}
            alignItems="center"
            justifyContent="center"
            // mb={10}
        >
            <Flex
                w="full"
                pos="relative"
                overflow="hidden"
                mt={"74px"}
            >
                <Flex
                    h={{lg: "1016px", md: "600px", sm: "300px"}}
                    w="full"
                    {...carouselStyle}
                >
                    {slides.map((slide, sid) => (
                        <Slide key={sid} slide={slide} sid={sid} slidesCount={slidesCount}/>
                    ))}
                </Flex>
                <Text {...arrowStyles} left="0" onClick={prevSlide}>
                    &#10094;
                </Text>
                <Text {...arrowStyles} right="0" onClick={nextSlide}>
                    &#10095;
                </Text>
                <HStack justify="center" pos="absolute" bottom="8px" w="full">
                    {Array.from({length: slidesCount}).map((_, slide) => (
                        <Box
                            key={`dots-${slide}`}
                            cursor="pointer"
                            boxSize={["7px", undefined, "15px"]}
                            m="0 2px"
                            bg={currentSlide === slide ? "blackAlpha.800" : "blackAlpha.500"}
                            rounded="50%"
                            display="inline-block"
                            transition="background-color 0.6s ease"
                            _hover={{bg: "blackAlpha.800"}}
                            onClick={() => setSlide(slide)}
                        />
                    ))}
                </HStack>
            </Flex>
        </Flex>
    );
};
export default Carousel;
