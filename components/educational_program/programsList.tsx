import React from "react";

import {
    Box,
    Flex,
    useColorModeValue,
} from "@chakra-ui/react";
import Program from "./program";

function ProgramsList({programs}) {

    return (
        <>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                p={{xl: 20, lg: 10, md: 10, sm: 5}}
                w="full"
                justifyContent="center"
                alignItems="center"
            >
                <Box
                    shadow="xl"
                    bg={useColorModeValue("white", "gray.800")}
                    p={{xl: 20, lg: 10, md: 10, sm: 5}}
                    mx="auto"
                    rounded={"lg"}
                    mt={10}
                    w={{lg: 1800, md: 800, sm: 600}}
                >
                    {programs.map(program => (
                        <Program
                            key={program.id}
                            program={program}
                        />
                    ))}
                </Box>
            </Flex>
        </>
    );
}


export async function getStaticProps() {
    const res = await fetch('http://localhost:3000/ensemble/educational_program')
    const programs = await res.json()

    return {
        props: {programs},
        revalidate: 10,
    }
}

export default ProgramsList


