import React from "react";
import {
    chakra,
    Box,
    useColorModeValue,
    SimpleGrid, Image,
} from "@chakra-ui/react";

const Program = ({program}) => {

    const gridColumnBox = program.fields.index % 2 ? 1 : 2

    return (
        <SimpleGrid
            alignItems="center"
            columns={{base: 1, xl: 2}}
            spacingX={{base: 10, md: 24}}
            mb={20}
        >
            <Image
                w="full"
                h="full"
                rounded={"lg"}
                fit="cover"
                shadow={"xl"}
                src={program.fields.image ? program.fields.image[0].url : '/85594d90-d45b-11e4-b0c7-003048346cae.jpeg'}
                mb={5}
            />
            <Box
                gridColumn={{base: 1, xl: gridColumnBox}}
                gridRow={{lg: 1, md: 2, sm: 2}}
            >
                <chakra.h2
                    mb={4}
                    fontSize={{base: "2xl", md: "4xl"}}
                    fontWeight="bold"
                    textAlign={{base: "center", md: "left"}}
                    color={useColorModeValue("gray.900", "gray.400")}
                >
                    {program.fields.title}
                </chakra.h2>
                <chakra.p
                    mb={5}
                    textAlign={{base: "center", sm: "left"}}
                    color={useColorModeValue("gray.600", "gray.400")}
                    fontSize={{md: "lg", sm: 'sm'}}
                >
                    {program.fields.body}
                </chakra.p>
            </Box>
        </SimpleGrid>
    );
};

export default Program;
