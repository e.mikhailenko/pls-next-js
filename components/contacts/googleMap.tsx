import React from "react";
import {AspectRatio, Box} from "@chakra-ui/react";

const GoogleMap = () => {
    return(
        <Box>
            <AspectRatio ratio={21 / 9}>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2911.9431441259617!2d131.8906601703079!3d43.12672057285105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5fb392059baaa273%3A0x77d825fcc5bca461!2z0JLQu9Cw0LTQuNCy0L7RgdGC0L7QutGB0LrQuNC5INCz0L7RgNC-0LTRgdC60L7QuSDQlNCy0L7RgNC10YYg0LTQtdGC0YHQutC-0LPQviDRgtCy0L7RgNGH0LXRgdGC0LLQsA!5e0!3m2!1sru!2sru!4v1621385507807!5m2!1sru!2sru"
                />
            </AspectRatio>
        </Box>
    )
}

export default GoogleMap
