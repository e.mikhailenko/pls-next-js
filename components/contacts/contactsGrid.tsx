import React from "react";
import {
    chakra,
    Box,
    Flex,
    useColorModeValue,
    Icon,
    Stack,
} from "@chakra-ui/react";
import {PhoneIcon} from '@chakra-ui/icons'
import {MdEmail, MdLocationOn} from "react-icons/md";

export default function ContactsGrid() {

    const PhoneIconConst = () => {
        return (
            <PhoneIcon
                fontSize={"lg"}
                h={7}
                w={7}
                mb={1}
                color={useColorModeValue("gray.900", "gray.100")}
            />
        )
    }

    const MdEmailConst = () => {
        return (
            <Icon
                fontSize={"lg"}
                as={MdEmail}
                h={7}
                w={7}
                mb={1}
                color={useColorModeValue("gray.900", "gray.100")}/>
        )
    }

    const MdLocationOnConst = () => {
        return (
            <Icon
                fontSize={"lg"}
                as={MdLocationOn}
                h={7}
                w={7}
                mb={1}
                color={useColorModeValue("gray.900", "gray.100")}/>
        )
    }


    const Feature = (props) => {
        return (
            <Flex>
                <Flex shrink={0}>
                    <Flex
                        alignItems="center"
                        justifyContent="center"
                        h={12}
                        w={12}
                        rounded="md"
                        color="white"
                    >
                        {props.icon}
                    </Flex>
                </Flex>
                <Box ml={3}>
                    <chakra.h1
                        fontSize="lg"
                        fontWeight="medium"
                        mt={2}
                        color={useColorModeValue("gray.900", "gray.300")}
                    >
                        {props.title}
                    </chakra.h1>
                </Box>
            </Flex>
        );
    };


    return (
        <Box>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                p={20}
                w="auto"
                justifyContent="center"
                alignItems="center"
            >
                <Box
                    py={12}
                    bg={useColorModeValue("white", "gray.900")}
                    rounded="xl"
                    shadow={"lg"}
                >
                    <Box maxW="7xl" mx="auto" px={{base: 4, lg: 8}}>
                        <Box textAlign={{lg: "center"}}>
                            <chakra.p
                                mt={2}
                                fontSize={{base: "3xl", sm: "4xl"}}
                                lineHeight="8"
                                fontWeight="extrabold"
                                letterSpacing="tight"
                                color={useColorModeValue("gray.900", "gray.500")}
                            >
                                Контакты
                            </chakra.p>
                        </Box>

                        <Box mt={10}>
                            <Stack
                                spacing={{base: 10, md: 0}}
                                display={{md: "grid"}}
                                gridTemplateColumns={{md: "repeat(2,1fr)"}}
                                gridColumnGap={{md: 8}}
                                gridRowGap={{md: 10}}
                            >
                                <Feature
                                    title="8 (423) 290-80-78"
                                    icon={PhoneIconConst()}
                                />

                                <Feature
                                    title="8 (924) 238-30-07"
                                    icon={PhoneIconConst()}
                                />

                                <Feature
                                    title="plyasunya@gmail.com"
                                    icon={MdEmailConst()}
                                />

                                <Feature
                                    title="г. Владивосток, Океанский пр-т 43"
                                    icon={MdLocationOnConst()}
                                />
                            </Stack>
                        </Box>
                    </Box>
                </Box>
            </Flex>
        </Box>
    );
}
