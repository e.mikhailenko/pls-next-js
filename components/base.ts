import Airtable from "airtable";

const airtableApiKey: string = process.env.AIRTABLE_API_KEY;
const airtableBaseId: string = process.env.AIRTABLE_BASE_ID;

export const base = new Airtable({apiKey: airtableApiKey}).base(airtableBaseId);
