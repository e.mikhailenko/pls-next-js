import Header from '../components/header'
import {Footer} from "../components/footer/footer";
import {AspectRatio, Box, chakra, Container, Flex, useColorModeValue} from "@chakra-ui/react";
import React, {useEffect, useState} from "react";
import {SimpleGrid} from "@chakra-ui/core";
import {base} from "../components/base";
import GalleryItems from "../components/gallery/galleryItems"

function Gallery() {

    const [galleryItems, setGalleryItems] = useState([])
    const [performances, setPerformances] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            await base('gallery')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setGalleryItems(records);
                    fetchNextPage();
                });
            await base('performances')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setPerformances(records);
                    fetchNextPage();
                });
        }
        fetchData().then();
    }, [])

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                pt={50}
                pb={50}
                w="full"
            >
                <Container centerContent mt={20}>
                    <SimpleGrid
                        columns={{xl: 3, lg: 2, md: 1, sm: 1}}
                        spacing={20}
                    >
                        <GalleryItems galleryItems={galleryItems} performances={performances}/>
                    </SimpleGrid>
                </Container>
            </Flex>
            <Footer/>
        </>
    )
}

export default Gallery
