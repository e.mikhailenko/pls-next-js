import Header from '../components/header'
import React, {useEffect, useState} from "react";
import Posts from "../components/blog/posts";
import {Footer} from "../components/footer/footer";
import {Flex, useColorModeValue, Container} from "@chakra-ui/react";
import {SimpleGrid} from "@chakra-ui/core";
import {base} from "../components/base";

function Blog() {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await base('posts')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setPosts(records);
                    fetchNextPage();
                });
        }
        fetchData().then();
    }, [])

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                pt={100}
                pb={100}
                w="full"
            >
                <Container centerContent mt={20}>
                    <SimpleGrid
                        columns={{xl: 3, lg: 2, md: 1, sm: 1}}
                        spacing="100px">
                        <Posts posts={posts}/>
                    </SimpleGrid>
                </Container>
            </Flex>
            <Footer/>
        </>
    );

}

export default Blog
