import {useRouter, withRouter} from 'next/router'
import Header from '../../../components/header'
import {Footer} from "../../../components/footer/footer";
import {base} from '../../../components/base';
import PostDetails from "../../../components/blog/postDetails";
import React, {useEffect, useState} from "react";
import {Container, Flex, useColorModeValue} from "@chakra-ui/react";
import {SimpleGrid} from "@chakra-ui/core";
import Posts from "../../../components/blog/posts";

const Post = () => {

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                pt={100}
                pb={100}
                w="full"
            >
                <Container
                    maxW={"6xl"}
                    centerContent
                >
                    <PostDetails/>
                </Container>
            </Flex>
            <Footer/>
        </>
    )
}

export default Post
