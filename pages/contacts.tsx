import Header from '../components/header'
import {Footer} from "../components/footer/footer";
import {Box, Container, Flex, useColorModeValue} from "@chakra-ui/react";
import React from "react";
import ContactsGrid from "../components/contacts/contactsGrid";
import GoogleMap from "../components/contacts/googleMap";

const Contacts = () => (
    <Box>
        <Header/>
        <Flex bg={useColorModeValue("#F9FAFB", "gray.600")}>
            <Container
                maxW={"full"}
                centerContent
                mt={20}
            >
                <ContactsGrid/>
            </Container>
        </Flex>
        <GoogleMap/>
        <Footer/>
    </Box>
)

export default Contacts
