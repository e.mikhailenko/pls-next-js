import Header from '../components/header'
import {Footer} from "../components/footer/footer";
import {Container, Flex, useColorModeValue} from "@chakra-ui/react";
import React, {useEffect, useState} from "react";
import {base} from "../components/base";
import {SimpleGrid} from "@chakra-ui/core";
import Cards from "../components/ensemble/cards";

function Ensemble() {

    const [cards, setCards] = useState([]);

    useEffect(() => {
        base('ensemble_cards')
            .select({view: 'Grid view'})
            .eachPage((records: any, fetchNextPage) => {
                setCards(records);
                fetchNextPage();
            });
    }, [])

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                pt={100}
                pb={100}
                w="full"
            >
                <Container centerContent>
                    <SimpleGrid
                        columns={1}
                        spacing="60px"
                        mt={20}
                    >
                        <Cards cards={cards}/>
                    </SimpleGrid>
                </Container>
            </Flex>
            <Footer/>
        </>
    );

}

export default Ensemble
