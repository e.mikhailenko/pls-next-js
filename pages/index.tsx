import Header from "../components/header";
import {Footer} from "../components/footer/footer";
import Carousel from "../components/carousel/carousel"
import React, {useEffect, useState} from "react";
import {base} from "../components/base";
import CallToAction from "../components/features/callToAction";
import SimpleThreeColumns from "../components/features/simpleThreeColumns";
import StructureImages from "../components/ensemble/structure/structureImages";


export default function IndexPage() {

    const [callToAction, setCallToAction] = useState([])
    const [preschoolers, setPreschoolers] = useState([])
    const [students, setStudents] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            await base('call_to_action').find('reccC4f7Gg6rYNTRx', function (err, record: any) {
                if (err) {
                    console.error(err)
                    return
                } else {
                    setCallToAction(record)
                }
            })
            await base('ensemble_structure').find('rec5YEvjgm8cEPTEY', function (err, record: any) {
                if (err) {
                    console.error(err)
                    return
                } else {
                    setPreschoolers(record)
                }
            })
            await base('ensemble_structure').find('rec3eeV2HjAPZIIwx', function (err, record: any) {
                if (err) {
                    console.error(err)
                    return
                } else {
                    setStudents(record)
                }
            })
        }
        fetchData().then();
    }, [])

    return (
        <>
            <Header/>
            <Carousel/>
            <SimpleThreeColumns/>
            <StructureImages structureImages={preschoolers}/>
            <StructureImages structureImages={students}/>
            <CallToAction callToAction={callToAction}/>
            <Footer/>
        </>
    )
}
