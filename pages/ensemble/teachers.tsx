import Header from '../../components/header'
import {Footer} from "../../components/footer/footer";
import {Container, Flex, useColorModeValue} from "@chakra-ui/react";
import React, {useEffect, useState} from "react";
import {base} from "../../components/base";
import {SimpleGrid} from "@chakra-ui/core";
import TeacherCards from "../../components/teachers/teacherCards";

function Teachers() {

    const [teachers, setTeachers] = useState([]);

    useEffect(() => {
        base('teachers')
            .select({view: 'Grid view'})
            .eachPage((records: any, fetchNextPage) => {
                setTeachers(records);
                fetchNextPage();
            });
    }, [])

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                pt={100}
                pb={100}
                w="full"
            >
                <Container centerContent>
                    <SimpleGrid
                        columns={{lg: 2, md: 1, sm: 1}}
                        spacing="60px"
                    >
                        <TeacherCards teachers={teachers}/>
                    </SimpleGrid>
                </Container>
            </Flex>
            <Footer/>
        </>
    );

}

export default Teachers
