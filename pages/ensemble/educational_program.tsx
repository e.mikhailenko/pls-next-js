import Header from '../../components/header'
import {Footer} from "../../components/footer/footer";
import {Container, Flex, useColorModeValue} from "@chakra-ui/react";
import React, {useEffect, useState} from "react";
import ProgramsList from "../../components/educational_program/programsList";
import {base} from "../../components/base";
import CallToAction from "../../components/features/callToAction";

function EducationalProgram() {

    const [programs, setPrograms] = useState([]);
    const [callToAction, setCallToAction] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            await base('educational_programs')
                .select({view: 'Grid view'})
                .eachPage((records: any, fetchNextPage) => {
                    setPrograms(records);
                    fetchNextPage();
                });
            await base('call_to_action').find('reccC4f7Gg6rYNTRx', function (err, record: any) {
                if (err) {
                    console.error(err)
                    return
                } else {
                    console.log('Retrieved', record.id);
                    setCallToAction(record)
                }
            })
        }
        fetchData().then();
    }, [])

    return (
        <>
            <Header/>
            <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
            >
                <Container
                    centerContent
                    maxW={"10xl"}
                    mt={20}
                >
                    <ProgramsList programs={programs}/>
                </Container>
            </Flex>
            <CallToAction callToAction={callToAction}/>
            <Footer/>
        </>
    )
}

export default EducationalProgram
