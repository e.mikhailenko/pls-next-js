import Header from '../components/header'
import {Footer} from "../components/footer/footer";
import {Flex, useColorModeValue, Container} from "@chakra-ui/react";
import React from "react";
import FeedbackForm from "../components/new_students/feedbackForm"

const NewStudents = () => (
    <>
        <Header/>
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            // w="full"
        >
            <Container
                maxW={{lg: "1000px", md: "500px"}}
                centerContent
            >
                <FeedbackForm/>
            </Container>
        </Flex>
        <Footer/>
    </>
)

export default NewStudents
